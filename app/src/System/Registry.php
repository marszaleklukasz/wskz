<?php

namespace App\System;

class Registry
{
    private static $collection = [];

    public static function get(string $key)
    {
        $item = null;

        if (array_key_exists($key, self::$collection)) {
            $item = self::$collection[$key];
        }

        return $item;
    }

    public static function set(string $key, $item)
    {
        self::$collection[$key] = $item;
    }
}

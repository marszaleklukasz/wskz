<?php

$routes = [
    '/api/generate' => ['controller' => 'ApiController', 'action' => 'generate'],
    '/api/retrieve' => ['controller' => 'ApiController', 'action' => 'retrieve']
];

<?php

namespace App\Lib;

class AuthService
{
    public static function verification(string $configValue, string $userValue)
    {
        return self::verificationHash($configValue, $userValue);
    }

    private static function verificationHash(string $configValue, string $userValue)
    {
        $result = false;

        $authorizationHash = self::getToken($userValue);

        if ($configValue === $authorizationHash) {
            $result = true;
        }

        return $result;
    }

    private static function getToken(string $userValue)
    {
        return str_replace('Bearer ', '', $userValue);
    }
}

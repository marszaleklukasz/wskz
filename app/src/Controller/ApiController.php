<?php

namespace App\Controller;

use App\System\HttpStatusCode;
use App\System\Response;
use App\Lib\RandomNumbers;
use App\Entity\RandomNumber;
use App\System\Registry;
use App\System\Model;

class ApiController
{
    private $data = null;
    private $dbConn = null;

    public function __construct($data)
    {
        $this->data = $data;
        $this->dbConn = Registry::get('dbConn');
    }

    public function generate()
    {
        $randomNumberValue = RandomNumbers::generate();

        $randomNumber = new RandomNumber();
        $randomNumber->setRandomNumber($randomNumberValue);

        $modelRandomNumber = new Model($randomNumber);

        $resultSave = $modelRandomNumber->save();

        if ($resultSave['id']) {
            Response::end(
                [
                    'statusCode' => HttpStatusCode::OK['code'],
                    'data' => [
                        'id' => $resultSave['id'],
                        'random_number' => $randomNumberValue
                    ]
                ],
                HttpStatusCode::OK['code']
            );
        } else {
            $message = HttpStatusCode::INTERNAL_SERVER_ERROR['name'];

            if (!empty($resultSave['error'][2])) {
                $message .= ', ' . $resultSave['error'][2];
            }

            throw new \Exception($message, HttpStatusCode::INTERNAL_SERVER_ERROR['code']);
        }
    }

    public function retrieve()
    {
        $id = null;

        if (!empty($this->data['parameters']['id'])) {
            $id = $this->data['parameters']['id'];
        }

        if (!empty($id)) {
            $randomNumber = new RandomNumber();

            $modelRandomNumber = new Model($randomNumber);

            $resultFindOneById = $modelRandomNumber->findOneById($id);

            if (!empty($resultFindOneById['error'][2])) {
                throw new \Exception(HttpStatusCode::INTERNAL_SERVER_ERROR['name'] . ', ' . $resultFindOneById['error'][2], HttpStatusCode::INTERNAL_SERVER_ERROR['code']);
            }

            if (!empty($resultFindOneById['data']['id']) && !empty($resultFindOneById['data']['random_number'])) {
                Response::end(
                    [
                        'statusCode' => HttpStatusCode::OK['code'],
                        'data' => [
                            'id' => $resultFindOneById['data']['id'],
                            'random_number' => $resultFindOneById['data']['random_number']
                        ]
                    ],
                    HttpStatusCode::OK['code']
                );
            } else {
                throw new \Exception('RandomNumber ' . HttpStatusCode::NOT_FOUND['name'], HttpStatusCode::NOT_FOUND['code']);
            }
        } else {
            throw new \Exception('RandomNumber ' . HttpStatusCode::BAD_REQUEST['name'], HttpStatusCode::BAD_REQUEST['code']);
        }
    }
}

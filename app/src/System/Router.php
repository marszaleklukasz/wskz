<?php

namespace App\System;

class Router
{
    private $routes = [];

    public function __construct($routes)
    {
        $this->routes = $routes;
    }

    public function getRoute(string $path)
    {
        $route = null;

        if (array_key_exists($path, $this->routes)) {
            $route = $this->routes[$path];
        }

        return $route;
    }
}

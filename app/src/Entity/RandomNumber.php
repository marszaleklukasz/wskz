<?php

namespace App\Entity;

class RandomNumber
{
    /**
     * @var string
     */
    public $table = 'random_number';

    /**
     * @var int
     */
    private ?int $id = null;

    /**
     * @var int
     */
    private ?int $randomNumber = null;

    /**
     * Set randomNumber.
     */
    public function setId(int $id): RandomNumber
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set randomNumber.
     */
    public function setRandomNumber(int $randomNumber): RandomNumber
    {
        $this->randomNumber = $randomNumber;

        return $this;
    }

    /**
     * Get randomNumber.
     */
    public function getRandomNumber(): ?int
    {
        return $this->randomNumber;
    }
}

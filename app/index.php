<?php

use App\System\Bootstrap;
use App\System\Response;
use App\System\HttpStatusCode;
use App\Lib\AuthService;
use App\System\Registry;

require './src/Lib/Autoloader.php';
require './config/routes.php';
require './config/db.php';
require './config/secret-code.php';

try {
    App\Lib\Autoloader::register();

    $headers = getallheaders();

    if (empty($headers['Authorization'])) {
        throw new \Exception(HttpStatusCode::UNAUTHORIZED['name'], HttpStatusCode::UNAUTHORIZED['code']);
    }

    $isValidAuth = AuthService::verification($secretCode['key'], $headers['Authorization']);

    if (!$isValidAuth) {
        throw new \Exception(HttpStatusCode::FORBIDDEN['name'], HttpStatusCode::FORBIDDEN['code']);
    }

    $dbConn = new PDO($dbParams['driver'] . ':host=' . $dbParams['host'] . ';port=' . $dbParams['port'] . ';dbname=' . $dbParams['dbname'], $dbParams['user'], $dbParams['password']);

    Registry::set('dbConn', $dbConn);

    $bootstrap = new Bootstrap($routes, $headers);

    $bootstrap->run();
} catch (Exception $exc) {
    $message = $exc->getMessage();
    $code = $exc->getCode();

    if (!in_array($code, HttpStatusCode::HttpStatusesSupported)) {
        $code = HttpStatusCode::INTERNAL_SERVER_ERROR['code'];
    }

    Response::end(
        [
            'statusCode' => $code,
            'message' => $message
        ],
        $code
    );
}

<?php

namespace App\System;

class Response
{
    public static function end($data, $code)
    {
        header('Content-type: application/json; charset=utf-8');
        http_response_code($code);

        echo json_encode($data);

        exit();
    }
}

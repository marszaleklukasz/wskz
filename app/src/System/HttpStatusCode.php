<?php

namespace App\System;

class HttpStatusCode
{
    const OK = ['code' => 200, 'name' => 'OK'];
    const BAD_REQUEST = ['code' => 400, 'name' => 'Bad Request'];
    const UNAUTHORIZED = ['code' => 401, 'name' => 'Unauthorized'];
    const FORBIDDEN = ['code' => 403, 'name' => 'Forbidden'];
    const NOT_FOUND = ['code' => 404, 'name' => 'Not Found'];
    const INTERNAL_SERVER_ERROR = ['code' => 500, 'name' => 'Internal Server Error'];

    const HttpStatusesSupported = [
        self::OK['code'],
        self::BAD_REQUEST['code'],
        self::UNAUTHORIZED['code'],
        self::FORBIDDEN['code'],
        self::NOT_FOUND['code'],
        self::INTERNAL_SERVER_ERROR['code']
    ];
}

<?php

namespace App\System;

use App\System\Registry;
use App\Lib\Utils;

class Model
{
    private $obj;
    private $properties;
    private $values;
    private $columns;
    private $dbConn;

    public function __construct(object $obj)
    {
        $this->obj = $obj;
        $this->properties = $this->getProperties($obj);
        $this->values = $this->getValues($obj);
        $this->columns = $this->getColumns($this->properties);

        $this->dbConn = Registry::get('dbConn');
    }

    public function findOneById(int $id)
    {
        $sql = $this->prepareFindOneByIdSql();

        $stmt = $this->dbConn->prepare($sql);

        $stmt->bindValue(':id', $id, \PDO::PARAM_INT);

        $stmt->execute();

        $result = $stmt->fetch(\PDO::FETCH_ASSOC);

        $error = $stmt->errorInfo();

        return [
            'data' => $result,
            'error' => $error
        ];
    }

    public function save(): array
    {
        $sql = $this->prepareInsertSql();

        $stmt = $this->dbConn->prepare($sql);

        $values = $this->getValuesWithoutId();

        $stmt->execute($values);

        $stmt->fetch();

        $error = $stmt->errorInfo();

        $id = $this->dbConn->lastInsertId();

        return [
            'id' => $id,
            'error' => $error
        ];
    }

    private function prepareFindOneByIdSql()
    {
        $sql = 'SELECT ' . implode(',', $this->columns) . ' FROM ' . $this->obj->table . ' WHERE id = :id';

        return $sql;
    }

    private function prepareInsertSql()
    {
        $values = $this->values;
        unset($values['id']);

        $properties = $this->properties;
        unset($properties[0]);

        $columns = $this->columns;
        unset($columns[0]);

        $placeholders = [];

        if (!empty($values)) {
            foreach (array_keys($values) as $value) {
                $placeholders[] =  ':' . $value;
            }
        }

        $sql = 'INSERT INTO ' . $this->obj->table . ' (' . implode(',', $columns) . ') VALUES (' . implode(',', $placeholders) . ')';

        return $sql;
    }

    private function getColumns($properties)
    {
        $columns = null;

        if (!empty($properties)) {
            foreach ($properties as $prop) {
                $columns[] = Utils::camelCaseToSnakeCase($prop);
            }
        }

        return $columns;
    }

    private function getProperties($obj)
    {
        $properties = [];

        $reflect = new \ReflectionClass($obj);

        $reflectProperties = $reflect->getProperties(\ReflectionProperty::IS_PRIVATE);

        if (!empty($reflectProperties)) {
            foreach ($reflectProperties as $prop) {
                $properties[] = $prop->getName();
            }
        }

        return $properties;
    }

    private function getValues($obj)
    {
        $values = [];

        if (!empty($this->properties)) {
            foreach ($this->properties as $prop) {
                $values[$prop] = $obj->{'get' . ucfirst($prop)}();
            }
        }

        return $values;
    }

    private function getValuesWithoutId()
    {
        $values = $this->values;
        unset($values['id']);

        return $values;
    }
}

# API

The API has two methods:
1. generate
2. retrieve

## Application Runtime

Use the docker-compose.


```bash
docker-compose up
```
OR
```bash
sudo docker-compose up
```

## An Example Of Endpoints
1. http://localhost:8500/api/generate
2. http://localhost:8500/api/retrieve?id=1

## Authorization
By adding the "Authorization" header, eg "Bearer xyzhjhjh343478hjjk87GFS532dsgfdhj3210kjkshj".

## phpMyAdmin
url: http://localhost:8181/

user: root

password: test

CREATE TABLE IF NOT EXISTS `random_number` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `random_number` bigint(20) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci

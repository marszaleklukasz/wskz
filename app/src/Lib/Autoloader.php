<?php

namespace App\Lib;

class Autoloader
{
    public static function register()
    {
        spl_autoload_register(function ($classPath)
        {

            $prefix = 'App';
            $baseDir = './src';

            if (substr($classPath, 0, strlen($prefix)) === $prefix) {
                $classPath = substr($classPath, strlen($prefix));

                $classPath = $baseDir . $classPath;
            }

            $classPath = str_replace('\\', DIRECTORY_SEPARATOR, $classPath);

            $file = $classPath . '.php';

            if (is_readable($file)) {
                require $file;

                return true;
            }

            return false;
        });
    }
}
